#!/bin/bash
set -o allexport; source .env; set +o allexport

echo "get cluster url"
URL=$(kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}') 
# Removing ANSI color codes from text stream
NEW_URL=$(echo $URL | sed 's/\x1b\[[0-9;]*m//g')
echo "$NEW_URL"
echo "$NEW_URL" > ./config/url.txt

echo "get PEM certificate"
echo $(kubectl get secret -o go-template='{{index .data "ca.crt" }}' $(kubectl get sa default -o go-template="{{range .secrets}}{{.name}}{{end}}")) | base64 -d > ./config/certificate.txt

cat ./config/certificate.txt




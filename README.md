# Civo Cluster and GitLab Integration

This project help you to setuo the Kubernetes integration between a **Civo** cluster and **GitLab** thanks to **GitPod**

This project is an "adaptation" of https://docs.gitlab.com/ee/user/clusters/management_project_template.html

> the project is here https://gitlab.com/gitlab-org/project-templates/cluster-management

## Getting started

- Set the `CIVO_API_KEY` environment variable in the GitPod's settings
- Import this project somewhere in GitLab and open it with GitPod
- Set the project to **private**
- Update the `.env` file with the appropriate values if needed

## Prerequisites

You need the the kubeconfig file: run this script `01-get-config.sh`

Then check the connection with this command: `kubectl get nodes` or `kubectl get pods --all-namespaces`

## Get data from the Civo cluster

### Certificate

Run `02-get-data.sh` and you will get in the `./config` directory:

- the certificate: `./config/certificate.txt`
- the url of the cluster: `./config/url.txt`

### Create the GitLab service account

Run `03-create-gitlab-service-account.sh` or use this command `kubectl apply -f gitlab-service-account.yaml`

### Get the access token

Run `04-get-access-token.sh`, the file will be generated in `./config/access.token.txt`

## Prepare your GitLab project or group

- Add Cluster Integration to the parent Group of this project
  - 🚧 to be detailed (https://docs.gitlab.com/ee/user/project/clusters/)
- Define this current project as a cluster management project
- Get the GitLab url and the runner token
- Update the values of `./applications/gitlab-runner/values.yaml`
- Update the content of `./helmfile.yaml` to install the needed application (I uncommented the gitlab runner and prometheus lines)
- Run the CI of the cluster management project



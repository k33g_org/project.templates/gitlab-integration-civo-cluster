#!/bin/bash
set -o allexport; source .env; set +o allexport

echo "generate access token"
#kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}') > ./config/token.txt

SECRET=$(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}')
CLUSTER_SECRET=$(kubectl -n kube-system get secret $SECRET -o jsonpath='{.data.token}' | base64 -d) 
echo ${CLUSTER_SECRET}> ./config/access.token.txt

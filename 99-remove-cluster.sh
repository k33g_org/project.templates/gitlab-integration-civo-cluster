#!/bin/bash
set -o allexport; source .env; set +o allexport

civo apikey add civo-key ${CIVO_API_KEY}
civo apikey current civo-key
civo kubernetes remove ${CLUSTER_NAME} --region=${CLUSTER_REGION} --yes 

rm ./config/access.token.txt
rm ./config/certificate.txt
rm ./config/k3s.yaml
rm ./config/url.txt
